﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication5
{
    class Program
    {
        static void Main(string[] args)
        {
            var terrain = new Terrain();
            var Gus = new Player("Gus", 3, 100, 5, 15);

            Console.WriteLine("You have decide to explore an abandoned castle. I wonder what secrets lie within...");
            Gus.Explore(terrain.returnStart());


        }
    }
}
