﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication5
{
    class Command
    {
        private List<string> commands;
        

        public Command()
        {
            commands = new List<string>();
            commands.Add("info");
            commands.Add("look");
            commands.Add("quit");
            commands.Add("go");
            commands.Add("help");
            commands.Add("take");
            commands.Add("drop");
            commands.Add("eat");
            commands.Add("enable");
            commands.Add("disable");
            commands.Add("exit");
           
        }

        public string[] readCommand()
        {
               string command = Console.ReadLine();
               string[] words = command.Split(' ');
               while (commands.Contains(words[0]) == false || command == null)
               {
                  Console.WriteLine("Please enter a valid command.");
                  command = Console.ReadLine();
                  words = command.Split(' ');
               }
  
               return words;
            
        }

        

        public void printCommands()
        {
            Console.WriteLine("The available commands are:");
            foreach (String s in commands)
            {
                Console.WriteLine("{0}", s);
            }
        }
    }
}
