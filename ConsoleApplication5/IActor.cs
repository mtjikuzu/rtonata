﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication5
{
    interface IActor
    {
        void Explore(ILocation l);
    }
}
