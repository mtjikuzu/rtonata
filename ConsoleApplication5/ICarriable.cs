﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication5
{
    interface ICarriable
    {
        string returnName();
        int use();
        bool switchLightOn();
        bool switchLightOff();
    }
}
