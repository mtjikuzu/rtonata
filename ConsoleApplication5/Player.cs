﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication5
{
    class Player:Actor
    {
        private int lives;
        private int energy;
        private int energyDecrease;
        private int capacity;
        private bool light;
        private List<ICarriable> items;

        public Player(string name, int lives, int energy, int decrease, int capacity)
        {
            setName(name);
            this.lives = lives;
            this.energy = energy;
            this.capacity = capacity;
            light = false;
            items = new List<ICarriable>();

        }

        public void useEnergy(string energy)
        { 
            foreach (ICarriable e in items)
            {
                if (e.returnName() == energy)
                {
                    energy = e.use() + energy;
                    items.Remove(e);
                }
            }
        }

        
        public bool hasRoom()
        {
            if (items.Count >= capacity) { Console.WriteLine("You do not have room to carry this."); return false; }
            else { return true; }
            
        }

        public void spendEnergy()
        {
            energy = energy - 5;
        }

        public bool hasEnergy()
        {
            if (energy >= 5) { return true; }
            else { return false;}
        }

        public void printItems()
        {
            if (items.Count > 0)
            {
                Console.WriteLine("You have the following items:");
                foreach (ICarriable i in items)
                {
                    Console.WriteLine(i.returnName());
                }
            }
            else { Console.WriteLine("You have no items."); }
        }



        public new void Explore(ILocation l)
        {
            setLocation(l);
            returnLocation().printEnter(this);
            var c = new Command();
            bool finished = false;
            while (finished == false)
            {
                var words = c.readCommand();
                finished = processCommand(words);
            }



        }



        public bool processCommand(string[] words)
        {
            string commandWord = "";
            string word2 = "";
            if (words.Length == 2)
            {
                commandWord = words[0];
                word2 = words[1];
            }
            else { commandWord = words[0]; }

            if (commandWord == "look") 
            {
                if (word2 == "north" && returnLocation().hasNeighbor(Direction.North) == true) { Console.WriteLine("North is the {0}", returnLocation().getNeighbor(Direction.North).returnName()); }
                if (word2 == "south" && returnLocation().hasNeighbor(Direction.South) == true) { Console.WriteLine("South is the {0}", returnLocation().getNeighbor(Direction.South).returnName()); }
                if (word2 == "east" && returnLocation().hasNeighbor(Direction.East) == true) { Console.WriteLine("East is the {0}", returnLocation().getNeighbor(Direction.East).returnName()); }
                if (word2 == "west" && returnLocation().hasNeighbor(Direction.West) == true) { Console.WriteLine("West is the {0}", returnLocation().getNeighbor(Direction.West).returnName()); }
                if (word2 == "up" && returnLocation().hasNeighbor(Direction.Up) == true) { Console.WriteLine("Up is the {0}", returnLocation().getNeighbor(Direction.Up).returnName()); }
                if (word2 == "down" && returnLocation().hasNeighbor(Direction.Down) == true) { Console.WriteLine("Down is the {0}", returnLocation().getNeighbor(Direction.Down).returnName()); }
                if (word2 == "here" || word2 == "") 
                {
                    if (returnLocation().hasDayLight() == true || light==true || returnLocation().isLightSource()==true)
                    {
                        Console.WriteLine("You are at the {0}", returnLocation().returnName());
                        Console.WriteLine(returnLocation().returnDescription());
                        returnLocation().printItems();
                        returnLocation().revealNeighbors();
                        if (returnLocation().isExit() == true) { Console.WriteLine("This location has an exit."); }
                    }
                    else { Console.WriteLine("It is pitch black here. You cannot see anything."); }
                }

            }

            if (commandWord == "quit") { return true; }


            if (commandWord == "go")
            {
                if (word2 == "north" && returnLocation().hasNeighbor(Direction.North) == true)
                {
                    if (returnLocation().returnLocked().Contains("north") == false)
                    {
                        if (hasEnergy() == true)
                        {
                            var x = returnLocation().getNeighbor(Direction.North);
                            setLocation(x);
                            Console.WriteLine("You are now at {0}.", returnLocation().returnName());
                            spendEnergy();
                            
                        }
                        else { Console.WriteLine("You do not have enough energy to move, please consume food for energy."); }
                    }
                    else { Console.WriteLine("This direction is locked. You need a key."); }
                }
               
                if (word2 == "south" && returnLocation().hasNeighbor(Direction.South) == true)
                {
                    if(returnLocation().returnLocked().Contains("south")==false)
                    {
                      if (hasEnergy() == true)
                      {
                        var x = returnLocation().getNeighbor(Direction.South);
                        setLocation(x);
                        Console.WriteLine("You are now at {0}.", returnLocation().returnName());
                        spendEnergy();
                      }
                      else { Console.WriteLine("You do not have enough energy to move, please consume food for energy.");}
                    
                    }
                    else { Console.WriteLine("This direction is locked. You need a key."); }
                }
                if (word2 == "east" && returnLocation().hasNeighbor(Direction.East) == true)
                {
                    if(returnLocation().returnLocked().Contains("east")==false)
                    {
                       if (hasEnergy() == true)
                        {
                            var x = returnLocation().getNeighbor(Direction.East);
                            setLocation(x);
                            Console.WriteLine("You are now at {0}.", returnLocation().returnName());
                          spendEnergy();
                        }
                        else { Console.WriteLine("You do not have enough energy to move, please consume food for energy.");}
                    }
                    else { Console.WriteLine("This direction is locked. You need a key."); }
                }
                if (word2 == "west" && returnLocation().hasNeighbor(Direction.West) == true)
                {
                    if (returnLocation().returnLocked().Contains("west")==false)
                    {
                        if (hasEnergy() == true)
                        {
                            var x = returnLocation().getNeighbor(Direction.West);
                            setLocation(x);
                            Console.WriteLine("You are now at {0}.", returnLocation().returnName());
                            spendEnergy();
                        }
                        else { Console.WriteLine("You do not have enough energy to move, please consume food for energy."); }
                    }
                    else { Console.WriteLine("This direction is locked. You need a key."); }

                }
                if (word2 == "up" && returnLocation().hasNeighbor(Direction.Up) == true)
                {
                    if (hasEnergy() == true)
                    {
                        var x = returnLocation().getNeighbor(Direction.Up);
                        setLocation(x);
                        Console.WriteLine("You are now at {0}.", returnLocation().returnName());
                        spendEnergy();
                    }
                    else { Console.WriteLine("You do not have enough energy to move, please consume food for energy."); }
                 }
                if (word2 == "down" && returnLocation().hasNeighbor(Direction.Down) == true)
                {
                    if (hasEnergy() == true)
                    {
                        var x = returnLocation().getNeighbor(Direction.Down);
                        setLocation(x);
                        Console.WriteLine("You are now at {0}.", returnLocation().returnName());
                        spendEnergy();
                    }
                    else { Console.WriteLine("You do not have enough energy to move, please consume food for energy."); }
                }
                if (returnLocation().isExit() == true) { Console.WriteLine("This location has an exit."); }
                

            }

            if (commandWord == "info")
            {
                Console.WriteLine("You are at the {0}", returnLocation().returnName());
                Console.WriteLine("You have {0} lives, {1} energy,", lives, energy);
                if (items.Count > 0)
                {
                    printItems();
                }
                
                

            }

            if (commandWord == "help") { var d = new Command(); d.printCommands(); }

            if (commandWord == "exit") 
            {
                if (returnLocation().isExit() == true)
                {
                    Console.WriteLine("You have found the exit. You leave without looking back. Press any key."); Console.ReadKey();
                }
                else { Console.WriteLine("This is not an exit."); }
            }

            if (commandWord == "take")
            {
                if (hasRoom() == true)
                {
                    foreach (ICarriable i in returnLocation().returnItems())
                    {
                        if (word2 == i.returnName())
                        {
                            items.Add(i);
                            returnLocation().removeItem(i);
                            Console.WriteLine("You have taken the {0}.", i.returnName());
                            return false;
                        }
                    }
                }
            }
            if (commandWord == "drop")
            {
                foreach (ICarriable i in items)
                {
                    if (word2 == i.returnName())
                    {
                        
                        returnLocation().addItem(i);
                        items.Remove(i);
                        Console.WriteLine("You have dropped {0}.", i.returnName());
                        return false;
                    }
                }
            }
            if (commandWord == "eat")
            {
                if (word2 != "flashlight" || word2 != "key")
                {
                    foreach (ICarriable i in items)
                    {
                        if (word2 == i.returnName())
                        {

                            energy = energy + i.use();
                            items.Remove(i);
                            Console.WriteLine("You have eaten {0} and receieved {1} energy.", i.returnName(), i.use());
                            return false;
                        }
                    }
                }
            }

            if (commandWord == "enable")
            {
                if (word2 == "light")
                {
                    if (returnLocation().hasSwitch() == true)
                    {
                        returnLocation().setLightSource(true);
                        Console.WriteLine("You found the light switch. Light shines through the room.");
                    }
                    else
                    {
                        Console.WriteLine("There is no light switch.");

                    }
                    

                }
                if (word2 == "flashlight")
                {
                    
                    foreach (ICarriable j in items)
                    {
                        if (word2 == j.returnName())
                        {
                            light = j.switchLightOn();
                            Console.WriteLine("Your flashlight is now on. You can see through the darkness.");
                            return false;

                        }
                    }
                    Console.WriteLine("You do not have a flashlight."); 

                }
                if (word2 == "key")
                {
                    foreach (ICarriable i in items)
                    {
                        if (word2 == i.returnName())
                        {
                            Console.WriteLine("Which direction would you like to unlock?");
                            string d = Console.ReadLine();
                            if (d == "north" && returnLocation().returnLocked().Contains("north")) { returnLocation().removeLock("north"); items.Remove(i); Console.WriteLine("You have unlocked direction {0}.", d); return false;}
                            if (d == "south" && returnLocation().returnLocked().Contains("south")) { returnLocation().removeLock("south"); items.Remove(i); Console.WriteLine("You have unlocked direction {0}.", d); return false;}
                            if (d == "east" && returnLocation().returnLocked().Contains("east")) { returnLocation().removeLock("east"); items.Remove(i); Console.WriteLine("You have unlocked direction {0}.", d); return false;}
                            if (d == "west" && returnLocation().returnLocked().Contains("west")) { returnLocation().removeLock("west"); items.Remove(i); Console.WriteLine("You have unlocked direction {0}.", d); return false;}
                            Console.WriteLine("You didn't enter a valid direction or the direction is already open."); return false;
                        }
                        Console.WriteLine("You didn't enter a valid direction or the direction is already open."); return false;
                    }
                    Console.WriteLine("You do not have a key."); 

                }

            }

            if (commandWord == "disable")
            {
                if (word2 == "light")
                {
                    if (returnLocation().hasSwitch() == true)
                    {
                        returnLocation().setLightSource(false);
                    }
                    else
                    {
                        Console.WriteLine("There is no light switch.");

                    }


                }
                if (word2 == "flashlight")
                {

                    foreach (ICarriable j in items)
                    {
                        if (word2 == j.returnName())
                        {
                            light = j.switchLightOff();
                            Console.WriteLine("Your flashlight is now off. You won't be able to see if it is dark.");
                            return false;

                        }
                    }
                    Console.WriteLine("You do not have a flashlight.");

                }

            }
            return false;
           
        }
    }
}
