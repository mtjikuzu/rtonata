﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication5
{
    class Terrain
    {
        
        private ILocation start;
        
        public Terrain()
        {
            
            ILocation cell1 = new Location("Great Entrance"); //Regular Locations
            ILocation cell2 = new Location("Main Hall");
            ILocation cell3 = new Location("Dining Area");
            ILocation cell4 = new Location("Long Hallway");
            ILocation cell5 = new Location("Loom Chamber");
            ILocation cell6 = new Location("Spinning Staircase");
            ILocation cell7 = new Location("Dreary Kitchen");
            ILocation cell8 = new Location("Recreation Area"); 
            ILocation cell9 = new Location("Steep Staircase");
            ILocation cell10 = new Location("Master Bedroom");
            ILocation cell11 = new Location("Damp Dungeon");// continues from spinning staircase
            ILocation cell12 = new Location("Broken Cells");
            ILocation cell13 = new Location("Armory");
            ILocation cell14 = new Location("Locked Door");
            ILocation cell15 = new Location("Graveyard");
            ILocation cell16 = new Location("The Easy Path");
            ILocation cell17 = new Location("The Mysterious Chamber");
            ILocation cell18 = new Location("The Outer Grounds");

            

            ILocation wcell1 = new LocationEn(cell1); //Location Wrappers
            wcell1.setDescription("A massive entrance of a castle fit for a king, now abandoned, and in shambles. There is an apple tree by the entrance.");
            ILocation wcell2 = new LocationEn(cell2);
            wcell2.setDescription("An open hall with a large chandelier hanging on by a few wires.");
            ILocation wcell3 = new LocationEn(cell3);
            wcell3.setDescription("A once elegent dining area, it is now filled with rotten wood tables and tattered paintings. Light barely shines in.");
            ILocation wcell4 = new LocationEn(cell4);
            wcell4.setDescription("A long corridor that gets darker as you venture down it.");
            ILocation wcell5 = new LocationEn(cell5);
            wcell5.setDescription("A dark damp room filled with cobwebs and bones....? As you examine further, you realize that the remains are human.");
            wcell5.setDayLight(false);
            wcell5.setHasLightSwitch(true);
            ILocation wcell6 = new LocationEn(cell6);
            wcell6.setDescription("A spinning staircase that goes lower into the abandoned castle. With each step down, the air grows colder and the rustling of rats can be heard.");
            wcell6.setDayLight(false);
            wcell6.setHasLightSwitch(true);
            ILocation wcell7 = new LocationEn(cell7);
            wcell7.setDescription("A smelly kitchen with rats scurrying away. The cabinets are all torn down. You see a refridgerator in the corner.");
            ILocation wcell8 = new LocationEn(cell8);
            wcell8.setDescription("A once lively area where guests would visit. Now the only guests are the rats.");
            ILocation wcell9 = new LocationEn(cell9);
            wcell9.setDescription("A steep staircase leading up to the second level.");
            ILocation wcell10 = new LocationEn(cell10);
            wcell10.setDescription("The master bedroom where a king slept. Pieces of furniture cover the floor. While kicking around the pieces, you reveal a shiny metal object.");
            ILocation wcell11 = new LocationEn(cell11);
            wcell11.setDescription("A cold, dark, and damp dungeon. Slight moans are heard. Is it your imagination?");
            wcell11.setDayLight(false);
            wcell11.setHasLightSwitch(true);

            ILocation wcell12 = new LocationEn(cell12);
            wcell12.setDescription("You find prison cells with broken doors. More human bones.");
            wcell12.setDayLight(false);

            ILocation wcell13 = new LocationEn(cell13);
            wcell13.setDescription("An armory cleaned out of weapons. Few remain but in terrible condition.");
            wcell13.setDayLight(false);
            ILocation wcell14 = new LocationEn(cell14);
            wcell14.setDescription("An ominous locked door amongest the so far empty castle. A sign on the door says TURN BACK.");
            wcell14.setDayLight(false);
            wcell14.addLock("north");
            ILocation wcell15 = new LocationEn(cell15);
            wcell15.setDescription("A small graveyard behind the castle. One tombstone is in pieces. You can barely make out the other as you read, Here lies KING Grum, There is nothing, What you will find is tragedy. Take the Easy Path.");
            ILocation wcell16 = new LocationEn(cell16);
            wcell16.setDescription("An open door leading out of the grounds toward the town.");
            wcell16.setExit(true);
            ILocation wcell17 = new LocationEn(cell17);
            wcell17.setDescription("A mysterious dark chamber.... You find fur hairs by the entrance. A massive skeleton is in shackles. Too large to be human but it has the same body structure. THe skull has large sharp teeth. You shine the flashlight around. A message is carved in the wall. FATHER HAS LEFT ME.");
            ILocation wcell18 = new LocationEn(cell18);
            wcell18.setDescription("The outside grounds behind the castle. You see the walls surrounding the grounds. There is a well.");

            start = wcell1;


            
            
            ICarriable e1 = new Energy("apple", 20);
            ICarriable e2 = new Flashlight();
            ICarriable e3 = new Energy("rotten-egg", -10);
            ICarriable e4 = new Energy("rat-meat", 25);
            ICarriable e5 = new Energy("water", 25);
            ICarriable e6 = new Energy("berries", 20);
            ICarriable e7 = new Energy("chocolate-bar", 20);
            ICarriable e8 = new Key();
            ICarriable e9 = new Energy("banana", 20);
            ICarriable e10 = new Key();
            ICarriable e11 = new Energy("water", 20);

            wcell1.addItem(e1);
            wcell2.addItem(e9);
            wcell3.addItem(e2);
            wcell5.addItem(e4);
            wcell7.addItem(e3);
            wcell18.addItem(e5);
            wcell15.addItem(e6);
            wcell10.addItem(e7);
            wcell10.addItem(e8);
            wcell11.addItem(e10);
            wcell11.addItem(e11);

            

            wcell1.addNeighbor(Direction.North, wcell2); //1 to 2

            wcell2.addNeighbor(Direction.South, wcell1); //Links cell 2 to 3,4
            wcell2.addNeighbor(Direction.North, wcell3);
            wcell2.addNeighbor(Direction.West, wcell4);
            

            wcell3.addNeighbor(Direction.South, wcell2); // vice versa
            wcell3.addNeighbor(Direction.North, wcell7);
            
            wcell4.addNeighbor(Direction.East, wcell2); 

            wcell4.addNeighbor(Direction.North, wcell5); //4 to 5

            wcell5.addNeighbor(Direction.South, wcell4); // vv
            

            wcell5.addNeighbor(Direction.West, wcell6); // 5 to 6 and 6 to 5
            wcell6.addNeighbor(Direction.East, wcell5);
            
            wcell6.addNeighbor(Direction.Down, wcell11);
            wcell11.addNeighbor(Direction.Up, wcell6);

            wcell7.addNeighbor(Direction.South, wcell3);
            wcell7.addNeighbor(Direction.East, wcell8);

            wcell8.addNeighbor(Direction.East, wcell18);
            wcell8.addNeighbor(Direction.West, wcell7);
            wcell8.addNeighbor(Direction.North, wcell9);
            wcell9.addNeighbor(Direction.South, wcell8);
            wcell9.addNeighbor(Direction.Up, wcell10);
            wcell10.addNeighbor(Direction.Down, wcell9);

            wcell11.addNeighbor(Direction.South, wcell12);

            wcell12.addNeighbor(Direction.North, wcell11);
            wcell12.addNeighbor(Direction.West, wcell13);
            wcell13.addNeighbor(Direction.East, wcell12);
            wcell13.addNeighbor(Direction.North, wcell14);

            wcell14.addNeighbor(Direction.South, wcell13);
            wcell14.addNeighbor(Direction.North, wcell17);

            wcell17.addNeighbor(Direction.South, wcell14);

            
            wcell18.addNeighbor(Direction.West, wcell8);
            wcell18.addNeighbor(Direction.North, wcell15);
            wcell15.addNeighbor(Direction.South, wcell18);

            wcell15.addNeighbor(Direction.North, wcell16);
            wcell15.addLock("north");
            wcell16.addNeighbor(Direction.South, wcell15);


            



        }

        public ILocation returnStart()
        {
            return start;
        }
    }
}
