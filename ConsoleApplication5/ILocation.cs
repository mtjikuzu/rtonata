﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication5
{
    interface ILocation
    {
        void revealNeighbors();
        void removeNeighbor(Direction d, ILocation l);
        void addNeighbor(Direction d, ILocation l);
        void allowEnter();
        void negateEnter();
        void allowExit();
        void negateExit();
        void printExit(Actor p);
        void printEnter(Actor p);
        string returnName();
        ILocation getNeighbor(Direction d);
        bool hasNeighbor(Direction d);

        List<ICarriable> returnItems();
        void addItem(ICarriable i);
        void removeItem(ICarriable i);
        bool isExit();
        void setExit(bool b);
        void setDayLight(bool b);
        bool hasDayLight();
        void setLightSource(bool b);
        bool hasSwitch();
        bool isLightSource();
        void setHasLightSwitch(bool b);
        void printItems();
        string returnDescription();
        void setDescription(string d);

        List<string> returnLocked();
        void addLock(string d);
        void removeLock(string d);
        
        
    }
}
