﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication5
{
    class LocationEn:ILocation
    {

        private ILocation L;
        private string description;
        private List<ICarriable> items;
        private bool hasExit;
        private bool dayLight;
        private bool lightSwitch;
        private bool lightSource;
        private List<string> locked;


        public LocationEn(ILocation l)
        {
            L = l;
            items = new List<ICarriable>();
            locked = new List<string>();
            hasExit = false;
            dayLight = true;
            lightSwitch = false;
            lightSource = false;
            description = "";
        }

        public void addNeighbor(Direction d, ILocation l)
        {
            L.addNeighbor(d, l);
        }

        public void removeNeighbor(Direction d, ILocation l)
        {
            L.removeNeighbor(d, l);

        }

        public bool isExit() { return hasExit; }
        public void setExit(bool b) { hasExit = b; }
        public void removeExit() { hasExit = false; }
        public bool hasDayLight() { return dayLight; }
        public void setDayLight(bool b) { dayLight = b; }
        public bool hasSwitch() { return lightSwitch; }
        public void setHasLightSwitch(bool b) { lightSwitch = b; }
        public bool isLightSource() { return lightSource; }
        public void setLightSource(bool b) { lightSource = b; }


        public void revealNeighbors()
        {
            L.revealNeighbors();
        }

        public void setDescription(string d)
        {
            description = d;
        }

        public string returnDescription() { return description; }

        public List<ICarriable> returnItems()
        {
            return items;
        }

        public List<string> returnLocked() { return locked; }
        public void addLock(string d) { locked.Add(d); }
        public void removeLock(string d) { locked.Remove(d); }


        public void printItems()
        {
            if (items.Count > 0)
            {
                Console.WriteLine("You found the following items here:");
                foreach (ICarriable i in items)
                {
                    Console.WriteLine(i.returnName());
                }
            }
            else { Console.WriteLine("There are no items."); }
        }
        public void addItem(ICarriable i) { items.Add(i); }
        public void removeItem(ICarriable i) { items.Remove(i); }

        public void allowEnter() { L.allowEnter(); }
        public void negateEnter() { L.negateEnter(); }
        public void allowExit() { L.allowExit(); }
        public void negateExit() { L.negateExit(); }

        public void printEnter(Actor p) { L.printEnter(p); }
        public void printExit(Actor p) { L.printExit(p); }

        public string returnName() { return L.returnName(); }
        public ILocation getNeighbor(Direction d) { return L.getNeighbor(d); }
        public bool hasNeighbor(Direction d) { return L.hasNeighbor(d); }
    }
}
