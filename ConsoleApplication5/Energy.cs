﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication5
{
    class Energy:ICarriable
    {
        private string name;
        private int energy;
        public Energy(string name, int energy)
        {
            this.name = name;
            this.energy = energy;
        }

        public string returnName()
        {
            return name;
        }

        public int use()
        {
            return energy;
        }

        public bool switchLightOn() { throw new NotImplementedException(); }
        public bool switchLightOff() { throw new NotImplementedException(); }
    }
}
