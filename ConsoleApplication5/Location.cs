﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication5
{
    public enum Direction
    {
        North,
        South,
        East,
        West,
        Up,
        Down
    }
    class Location:ILocation
    {

        private Dictionary<Direction, ILocation> connect;
        private bool enter;
        private bool exit;
        private string name;


        public Location(string name)
        {
            connect = new Dictionary<Direction, ILocation>();
            enter = true;
            exit = true;
            this.name = name;
        }

        public void addNeighbor(Direction d, ILocation l)
        {
            connect.Add(d, l);
            
        }

        public void removeNeighbor(Direction d, ILocation l)
        {

            if (connect.ContainsKey(d) == true)
            {
                connect.Remove(d);
            }

        }

        public void revealNeighbors()
        {
            foreach (Direction x in connect.Keys)
            {
                Console.WriteLine("{0} is the {1}",x,connect[x].returnName());
            }
        }
        public string returnName()
        {
            return name;
        }

        public ILocation getNeighbor(Direction d)
        {
            return connect[d];
        }

        public bool hasNeighbor(Direction d)
        {
            if (connect.ContainsKey(d)) { return true; }
            else { return false; }
        }


        public string getName() { return name; }
        public void allowEnter() { enter = true; }
        public void negateEnter() { enter = false; }
        public void allowExit() { exit = true; }
        public void negateExit() { exit = false; }

        public void printEnter(Actor p)
        {
            Console.WriteLine("Player {0} has entered the {1}.", p.returnName(), name); 
        }

        public void printExit(Actor p)
        {
            Console.WriteLine("Player {0} has left the {1}.", p.returnName(), name); 
        }

        public List<ICarriable> returnItems() { throw new NotImplementedException(); }
        public void addItem(ICarriable i) { throw new NotImplementedException(); }
        public void removeItem(ICarriable i) { throw new NotImplementedException(); }
        public void printItems() { throw new NotImplementedException(); }
        public bool isExit() { throw new NotImplementedException(); }
        public void setDayLight(bool b) { throw new NotImplementedException(); }
        public bool hasDayLight() { throw new NotImplementedException(); }
        public void setLightSource(bool b) { }
        public bool hasSwitch() { throw new NotImplementedException(); }
        public bool isLightSource() {throw new NotImplementedException();}
        public void setHasLightSwitch(bool b) { throw new NotImplementedException(); }
        public string returnDescription() { throw new NotImplementedException(); }
        public void setDescription(string d) { throw new NotImplementedException(); }
        public List<string> returnLocked() { throw new NotImplementedException(); }
        public void addLock(string d) { throw new NotImplementedException(); }
        public void removeLock(string d) { throw new NotImplementedException(); }
        public void setExit(bool b) { throw new NotImplementedException(); }



        




    }
}
