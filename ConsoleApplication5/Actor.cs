﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication5
{
    abstract class Actor:IActor
    {
        private ILocation current;
        private string name;
        public Actor()
        {
            
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public string returnName()
        {
            return name;
        }

        public ILocation returnLocation()
        {
            return current;
        }

        public void setLocation(ILocation l)
        {
            current = l;
        }
        public void Explore(ILocation l)
        {
            
            
        }

       
    }
}
